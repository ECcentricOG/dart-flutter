class Task {
  final String? title;
  final String? data;
  final String? date;

  const Task({required this.title, required this.data, required this.date});
}

List allTasks = [
  const Task(
    title: "ECcentricOG",
    data: "Complete God of Wars",
    date: "23 Dec 2023",
  ),
];
