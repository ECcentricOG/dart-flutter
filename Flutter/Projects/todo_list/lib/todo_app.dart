import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:google_fonts/google_fonts.dart';
import 'task.dart';

class ToDoApp extends StatefulWidget {
  const ToDoApp({super.key});

  @override
  State<ToDoApp> createState() => _ToDoAppState();
}

class _ToDoAppState extends State<ToDoApp> {
  int taskCount = 0;

  TextEditingController title = TextEditingController();
  TextEditingController data = TextEditingController();
  TextEditingController date = TextEditingController();

  Color colorCard(int index) {
    if (index % 3 == 0) {
      return const Color.fromRGBO(232, 237, 250, 1);
    } else if (index % 4 == 1) {
      return const Color.fromRGBO(250, 232, 232, 1);
    } else {
      return const Color.fromRGBO(250, 249, 232, 1);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "To-do list",
          style: GoogleFonts.quicksand(
            color: Colors.white,
            fontSize: 26,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
      ),
      body: ListView.separated(
        scrollDirection: Axis.vertical,
        itemCount: allTasks.length,
        separatorBuilder: (context, index) => const Divider(
          height: 2,
        ),
        itemBuilder: (context, index) {
          return SizedBox(
            width: 330,
            child: Card(
              color: colorCard(index),
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        height: 60,
                        width: 60,
                        margin: const EdgeInsets.only(
                            left: 5, top: 5, bottom: 0, right: 3),
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        child: Image.asset(
                          "assets/Group 42.png",
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Column(
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.7,
                            child: Text(
                              allTasks[index].title,
                              style: GoogleFonts.quicksand(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.7,
                            child: Text(
                              allTasks[index].data,
                              style: GoogleFonts.quicksand(
                                fontSize: 10,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        allTasks[index].date,
                        style: GoogleFonts.quicksand(
                          fontSize: 10,
                          color: Colors.grey,
                        ),
                      ),
                      const Spacer(),
                      SizedBox(
                        child: IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.mode_edit_outline_outlined),
                          color: const Color.fromRGBO(0, 139, 148, 1),
                          tooltip: "Edit task",
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(right: 5, left: 2),
                        child: IconButton(
                          onPressed: () {
                            allTasks.removeAt(index);
                            setState(() {});
                          },
                          icon: const Icon(Icons.delete_outline_rounded),
                          color: const Color.fromRGBO(0, 139, 148, 1),
                          tooltip: "Remove task",
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
            backgroundColor: Colors.white,
            context: context,
            builder: (context) {
              return Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: title,
                    keyboardType: TextInputType.name,
                    decoration: const InputDecoration(
                      hintText: "Title",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: data,
                    keyboardType: TextInputType.name,
                    decoration: const InputDecoration(
                      hintText: "Task",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: date,
                    keyboardType: TextInputType.datetime,
                    decoration: const InputDecoration(
                      hintText: "Date",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      allTasks.add(Task(
                          title: title.text, data: data.text, date: date.text));
                      title.clear();
                      data.clear();
                      date.clear();
                      setState(() {});
                      Navigator.pop(context);
                    },
                    style: ButtonStyle(
                      fixedSize: MaterialStateProperty.all(const Size(300, 50)),
                      backgroundColor: const MaterialStatePropertyAll(
                        Color.fromRGBO(0, 139, 148, 1),
                      ),
                    ),
                    child: const Text(
                      "Submit",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              );
            },
          );
        },
        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }
}
