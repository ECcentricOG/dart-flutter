import 'package:advance_to_do_list/model.dart';
import 'package:advance_to_do_list/todo_list.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

dynamic database;
void main() async {
  runApp(const MainApp());
  database = await openDatabase(
    join(await getDatabasesPath(), "MyToDoDB.db"),
    version: 1,
    onCreate: (db, version) async {
      db.execute(
        '''
        CREATE TABLE activity (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          title TEXT,
          description TEXT,
          date TEXT
        )
        ''',
      );
    },
  );
  await allToList();
  print(activities.length);
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ToDoApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}
