import 'package:advance_to_do_list/sqflite_database.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:slideable/slideable.dart';
import 'package:advance_to_do_list/model.dart';

class ToDoApp extends StatefulWidget {
  const ToDoApp({super.key});

  @override
  State<ToDoApp> createState() => _ToDoAppState();
}

class _ToDoAppState extends State<ToDoApp> {
  TextEditingController title = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController date = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  Color isChecked(int index) {
    if (activities[index].isChecked) {
      return Colors.greenAccent;
    } else {
      return Colors.transparent;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(111, 81, 255, 1),
      ),
      backgroundColor: const Color.fromRGBO(111, 81, 255, 1),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(left: 20),
            child: Text(
              "Good Morning",
              style: GoogleFonts.quicksand(
                fontSize: 22,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 20, bottom: 30),
            child: Text(
              "Umesh",
              style: GoogleFonts.quicksand(
                fontSize: 30,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            decoration: const BoxDecoration(
              color: Color.fromRGBO(217, 217, 217, 1),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40), topRight: Radius.circular(40)),
            ),
            height: MediaQuery.of(context).size.height - 209.5,
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "TO DO LIST",
                  style: GoogleFonts.quicksand(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40)),
                  ),
                  height: MediaQuery.of(context).size.height - 267,
                  child: ListView.builder(
                    itemCount: activities.length,
                    itemBuilder: (context, index) {
                      return Slideable(
                        resetSlide: true,
                        curve: Curves.ease,
                        duration: Durations.short1,
                        items: [
                          ActionItems(
                            icon: const Icon(
                              Icons.mode_edit_outlined,
                              color: Color.fromRGBO(111, 81, 255, 1),
                            ),
                            onPress: () {
                              String? titleEdit = activities[index].title;
                              String? desEdit = activities[index].description;
                              String? dateEdit = activities[index].date;
                              showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Form(
                                    key: _formKey,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        Container(
                                          margin:
                                              const EdgeInsets.only(left: 20),
                                          child: Text(
                                            "Title",
                                            style: GoogleFonts.quicksand(
                                              fontSize: 11,
                                              color: const Color.fromRGBO(
                                                  111, 81, 255, 1),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          child: Padding(
                                            padding: const EdgeInsets.all(3),
                                            child: TextFormField(
                                              initialValue: titleEdit,
                                              onChanged: (value) {
                                                titleEdit = value;
                                              },
                                              validator: (value) {
                                                if (value.toString().isEmpty) {
                                                  return "Title is required";
                                                } else {
                                                  return null;
                                                }
                                              },
                                              keyboardType: TextInputType.name,
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(40),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin:
                                              const EdgeInsets.only(left: 20),
                                          child: Text(
                                            "Description",
                                            style: GoogleFonts.quicksand(
                                              fontSize: 11,
                                              color: const Color.fromRGBO(
                                                  111, 81, 255, 1),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          child: Padding(
                                            padding: const EdgeInsets.all(3),
                                            child: TextFormField(
                                              key:
                                                  const ValueKey("description"),
                                              initialValue: desEdit,
                                              onChanged: (newValue) {
                                                desEdit = newValue;
                                              },
                                              validator: (value) {
                                                if (value.toString().isEmpty) {
                                                  return "Description is required";
                                                } else {
                                                  return null;
                                                }
                                              },
                                              keyboardType: TextInputType.name,
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(40),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin:
                                              const EdgeInsets.only(left: 20),
                                          child: Text(
                                            "Date",
                                            style: GoogleFonts.quicksand(
                                              fontSize: 11,
                                              color: const Color.fromRGBO(
                                                  111, 81, 255, 1),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          child: Padding(
                                            padding: const EdgeInsets.all(3),
                                            child: TextFormField(
                                              key: const ValueKey("date"),
                                              initialValue: dateEdit,
                                              onChanged: (newValue) {
                                                dateEdit = newValue;
                                              },
                                              validator: (value) {
                                                if (value.toString().isEmpty) {
                                                  return "Date is required";
                                                } else {
                                                  return null;
                                                }
                                              },
                                              keyboardType:
                                                  TextInputType.datetime,
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(
                                                  borderSide: const BorderSide(
                                                    color: Color.fromRGBO(
                                                        111, 81, 255, 1),
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(40),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        Center(
                                          child: ElevatedButton(
                                            onPressed: () async {
                                              final isValid = _formKey
                                                  .currentState!
                                                  .validate();
                                              if (isValid) {
                                                activities[index].title =
                                                    titleEdit;
                                                activities[index].date =
                                                    dateEdit;
                                                activities[index].description =
                                                    desEdit;
                                                update(activities[index]);
                                                await allToList();
                                                setState(() {});
                                                Navigator.pop(context);
                                              }
                                            },
                                            style: const ButtonStyle(
                                              fixedSize:
                                                  MaterialStatePropertyAll(
                                                      Size(300, 50)),
                                              backgroundColor:
                                                  MaterialStatePropertyAll(
                                                Color.fromRGBO(111, 81, 255, 1),
                                              ),
                                            ),
                                            child: Text(
                                              "Submit",
                                              style: GoogleFonts.inter(
                                                color: Colors.white,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              );
                            },
                            radius: BorderRadius.circular(32),
                            backgroudColor: Colors.white,
                          ),
                          ActionItems(
                            icon: const Icon(
                              Icons.delete_outline_rounded,
                              color: Color.fromRGBO(111, 81, 255, 1),
                            ),
                            onPress: () async {
                              activities.remove(activities[index]);
                              await allToList();
                              setState(() {});
                            },
                            radius: BorderRadius.circular(32),
                            backgroudColor: Colors.white,
                          ),
                        ],
                        child: Container(
                          height: 90,
                          margin: const EdgeInsets.all(7),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(40),
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(217, 217, 217, 1),
                                blurRadius: 7,
                                spreadRadius: 2,
                              ),
                            ],
                          ),
                          child: Row(
                            children: [
                              Container(
                                width: 52,
                                height: 52,
                                margin: const EdgeInsets.only(
                                  left: 15,
                                  right: 15,
                                  top: 3,
                                  bottom: 3,
                                ),
                                decoration: const BoxDecoration(
                                  color: Colors.grey,
                                  shape: BoxShape.circle,
                                ),
                              ),
                              Column(
                                children: [
                                  const SizedBox(
                                    height: 12,
                                  ),
                                  SizedBox(
                                    width: 237,
                                    child: Text(
                                      "${activities[index].title}",
                                      maxLines: 1,
                                      style: GoogleFonts.inter(
                                        fontSize: 11,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  SizedBox(
                                    width: 237,
                                    child: Text(
                                      "${activities[index].description}",
                                      maxLines: 3,
                                      style: GoogleFonts.inter(
                                        fontSize: 9,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                  Container(
                                    margin: const EdgeInsets.only(bottom: 3),
                                    child: Text(
                                      "${activities[index].date}",
                                      style: GoogleFonts.inter(
                                        fontSize: 8,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              const Spacer(),
                              Container(
                                margin: const EdgeInsets.only(right: 5),
                                child: Checkbox(
                                  shape: const CircleBorder(),
                                  value: activities[index].isChecked,
                                  activeColor: isChecked(index),
                                  onChanged: (value) {
                                    activities[index].isChecked =
                                        !activities[index].isChecked;
                                    setState(() {});
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
            context: context,
            builder: (context) {
              return Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 20),
                      child: Text(
                        "Title",
                        style: GoogleFonts.quicksand(
                          fontSize: 11,
                          color: const Color.fromRGBO(111, 81, 255, 1),
                        ),
                      ),
                    ),
                    SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.all(3),
                        child: TextFormField(
                          key: const ValueKey("title"),
                          controller: title,
                          validator: (value) {
                            if (value.toString().isEmpty) {
                              return "Title is required";
                            } else {
                              return null;
                            }
                          },
                          keyboardType: TextInputType.name,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(40),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 20),
                      child: Text(
                        "Description",
                        style: GoogleFonts.quicksand(
                          fontSize: 11,
                          color: const Color.fromRGBO(111, 81, 255, 1),
                        ),
                      ),
                    ),
                    SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.all(3),
                        child: TextFormField(
                          key: const ValueKey("description"),
                          controller: description,
                          validator: (value) {
                            if (value.toString().isEmpty) {
                              return "Description is required";
                            } else {
                              return null;
                            }
                          },
                          keyboardType: TextInputType.name,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(40),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 20),
                      child: Text(
                        "Date",
                        style: GoogleFonts.quicksand(
                          fontSize: 11,
                          color: const Color.fromRGBO(111, 81, 255, 1),
                        ),
                      ),
                    ),
                    SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.all(3),
                        child: TextFormField(
                          key: const ValueKey("date"),
                          controller: date,
                          validator: (value) {
                            if (value.toString().isEmpty) {
                              return "Date is required";
                            } else {
                              return null;
                            }
                          },
                          keyboardType: TextInputType.datetime,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                color: Color.fromRGBO(111, 81, 255, 1),
                              ),
                              borderRadius: BorderRadius.circular(40),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: ElevatedButton(
                        onPressed: () async {
                          final isValid = _formKey.currentState!.validate();
                          if (isValid) {
                            Activity a = Activity(
                              title: title.text,
                              description: description.text,
                              date: date.text,
                            );
                            insert(a);
                            await allToList();
                            title.clear();
                            date.clear();
                            description.clear();
                            setState(() {});
                            Navigator.pop(context);
                          }
                        },
                        style: const ButtonStyle(
                          fixedSize: MaterialStatePropertyAll(Size(300, 50)),
                          backgroundColor: MaterialStatePropertyAll(
                            Color.fromRGBO(111, 81, 255, 1),
                          ),
                        ),
                        child: Text(
                          "Submit",
                          style: GoogleFonts.inter(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
        backgroundColor: const Color.fromRGBO(111, 81, 255, 1),
        child: const Icon(Icons.add),
      ),
    );
  }
}
