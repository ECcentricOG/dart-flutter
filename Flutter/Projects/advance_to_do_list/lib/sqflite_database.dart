import 'package:advance_to_do_list/main.dart';
import 'package:advance_to_do_list/model.dart';
import 'package:sqflite/sqlite_api.dart';

Future<void> insert(Activity activity) async {
  dynamic localDB = await database;
  await localDB.insert(
    "activity",
    activity.toMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<List<Activity>> getActivities() async {
  dynamic localDB = await database;
  List<Map<String, dynamic>> list = await localDB.query("activity");

  return List.generate(
    list.length,
    (index) {
      return Activity(
        title: list[index]['title'],
        description: list[index]['description'],
        date: list[index]['date'],
        id: list[index]['id'],
      );
    },
  );
}

Future<void> delete(Activity activity) async {
  dynamic localDB = await database;
  await localDB.delete(
    "activity",
    where: "id = ?",
    whereArgs: [activity.id],
  );
}

Future<void> update(Activity activity) async {
  dynamic localDB = await database;
  await localDB.update(
    "activity",
    activity.toMap(),
    where: "id = ?",
    whereArgs: [activity.id],
  );
}
