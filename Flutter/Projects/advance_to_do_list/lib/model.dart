import 'package:advance_to_do_list/sqflite_database.dart';

class Activity {
  int? id;
  String? title;
  String? description;
  String? date;
  bool isChecked = false;

  Activity({
    this.id,
    required this.title,
    required this.description,
    required this.date,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'description': description,
      'date': date,
    };
  }
}

List<Activity> activities = List.empty(growable: true);

Future allToList() async {
  activities.clear();
  List<Activity> list = await getActivities();
  for (Activity a in list) {
    activities.add(a);
  }
}
