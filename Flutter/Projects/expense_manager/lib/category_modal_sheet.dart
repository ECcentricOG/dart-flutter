import 'package:expense_manager/controllers/controllers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Future categoriesModalSheet(BuildContext context) {
  return showModalBottomSheet(
    context: context,
    builder: (context) => Form(
      child: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Column(
            children: [
              Container(
                margin: const EdgeInsets.only(top: 10),
                height: 97,
                width: 74,
                decoration: const BoxDecoration(
                  color: Color.fromRGBO(140, 128, 128, 0.2),
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage("assets/Group 45.png"),
                  ),
                ),
              ),
              Center(
                child: Text(
                  "Add",
                  style: GoogleFonts.poppins(
                    fontSize: 13,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: const EdgeInsets.only(
              left: 20,
              right: 20,
              bottom: 12,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Image URL",
                  style: GoogleFonts.poppins(
                    fontSize: 13,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  height: 40,
                  child: TextFormField(
                    cursorHeight: 25,
                    controller: categoryUrl,
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.all(5),
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
              left: 20,
              right: 20,
              bottom: 12,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Category",
                  style: GoogleFonts.poppins(
                    fontSize: 13,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  height: 40,
                  child: TextFormField(
                    cursorHeight: 25,
                    controller: categoryName,
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.all(5),
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: const EdgeInsets.all(10),
            height: 40,
            width: 123,
            decoration: BoxDecoration(
              color: const Color.fromRGBO(14, 161, 125, 1),
              borderRadius: BorderRadius.circular(20),
            ),
            child: TextButton(
              onPressed: () {},
              child: Text(
                "Add",
                style: GoogleFonts.poppins(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
