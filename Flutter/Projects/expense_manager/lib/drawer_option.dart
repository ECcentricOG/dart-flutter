import 'package:expense_manager/views/about_us_page.dart';
import 'package:expense_manager/views/categories_page.dart';
import 'package:expense_manager/views/graph_page.dart';
import 'package:expense_manager/views/home_page.dart';
import 'package:expense_manager/views/trash_page.dart';
import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';

Drawer getDrawer(BuildContext context) {
  return Drawer(
    width: 216,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10,
        ),
        Center(
          child: Column(
            children: [
              Text(
                "Expense Manager",
                style: GoogleFonts.poppins(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Text(
                "Saves all your Transactions",
                style: GoogleFonts.poppins(
                  fontSize: 10,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          height: 40,
          width: 184,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
          child: TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const HomePage(),
                ),
              );
            },
            child: Row(
              children: [
                Image.asset("assets/transactions.png"),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  "Transaction",
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 12,
        ),
        Container(
          height: 40,
          width: 184,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
          child: TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const GraphPage(),
                ),
              );
            },
            child: Row(
              children: [
                Image.asset("assets/🦆 icon _pie chart_.png"),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  "Graphs",
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 12,
        ),
        Container(
          height: 40,
          width: 184,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
          child: TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return const CategoriesPage();
                  },
                ),
              );
            },
            child: Row(
              children: [
                Image.asset("assets/label.png"),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  "Category",
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 12,
        ),
        Container(
          height: 40,
          width: 184,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
          child: TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const TrashPage(),
                ),
              );
            },
            child: Row(
              children: [
                Image.asset("assets/delete.png"),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  "Trash",
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 12,
        ),
        Container(
          height: 40,
          width: 184,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
          child: TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const AboutUsPage(),
                ),
              );
            },
            child: Row(
              children: [
                Image.asset("assets/person.png"),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  "About us",
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}
