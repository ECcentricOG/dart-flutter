import 'package:expense_manager/drawer_option.dart';
import 'package:expense_manager/model/data_map.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pie_chart/pie_chart.dart';

class GraphPage extends StatefulWidget {
  const GraphPage({super.key});

  @override
  State<GraphPage> createState() => _GraphPageState();
}

class _GraphPageState extends State<GraphPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Graphs",
          style: GoogleFonts.poppins(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: Colors.black,
          ),
        ),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 30,
          ),
          PieChart(
            dataMap: data,
            animationDuration: const Duration(
              seconds: 2,
            ),
            chartValuesOptions: const ChartValuesOptions(
              showChartValues: false,
            ),
            legendOptions: const LegendOptions(
              legendShape: BoxShape.rectangle,
            ),
            chartType: ChartType.ring,
            ringStrokeWidth: 35,
            chartRadius: MediaQuery.of(context).size.width * 0.4,
            centerWidget: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Total",
                  style: GoogleFonts.poppins(
                    fontSize: 10,
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "₹ 2550.00",
                  style: GoogleFonts.poppins(
                    fontSize: 13,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      drawer: getDrawer(context),
    );
  }
}
