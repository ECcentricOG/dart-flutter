import 'package:expense_manager/dalete_category_function.dart';
import 'package:expense_manager/drawer_option.dart';
import 'package:expense_manager/model/category_model.dart';
import 'package:expense_manager/category_modal_sheet.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CategoriesPage extends StatefulWidget {
  const CategoriesPage({super.key});

  @override
  State<CategoriesPage> createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Categories",
          style: GoogleFonts.poppins(
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      body: GridView.builder(
        itemCount: categories.length,
        padding: const EdgeInsets.all(10),
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (context, index) => GestureDetector(
          //
          //Functions
          //
          onTap: () {},
          onLongPress: () async {
            await showDeleteDialog(context);
          },
          child: Container(
            margin: const EdgeInsets.all(10),
            height: 150,
            width: 150,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              boxShadow: const [
                BoxShadow(
                  offset: Offset(1, 2),
                  blurRadius: 8,
                  color: Color.fromRGBO(0, 0, 0, 0.15),
                ),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  height: 74,
                  width: 74,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: AssetImage(categories[index].url),
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    categories[index].name,
                    style: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.white,
        onPressed: () {
          categoriesModalSheet(context);
        },
        label: Row(
          children: [
            Container(
              margin: const EdgeInsets.only(right: 10),
              height: 32,
              width: 32,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(14, 161, 125, 1),
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage("assets/Group 15.png"),
                ),
              ),
            ),
            Text(
              "Add Category",
              style: GoogleFonts.poppins(
                fontSize: 12,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
      drawer: getDrawer(context),
    );
  }
}
