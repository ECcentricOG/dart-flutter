import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Future showDeleteDialog(BuildContext context) {
  return showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(
          "Delete Category",
          style: GoogleFonts.poppins(
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
        ),
        content: Text(
          "Are you sure you want to delete the selected category?",
          style: GoogleFonts.poppins(
            fontSize: 12,
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: [
          Container(
            width: 100,
            height: 35,
            decoration: BoxDecoration(
              color: const Color.fromRGBO(14, 161, 125, 1),
              borderRadius: BorderRadius.circular(20),
            ),
            child: TextButton(
              //
              //function
              //
              onPressed: () {},
              child: Text(
                "Delete",
                style: GoogleFonts.poppins(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Container(
            width: 100,
            height: 35,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
            ),
            child: TextButton(
              //
              //function
              //
              onPressed: () {},
              child: Text(
                "cancle",
                style: GoogleFonts.poppins(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ],
      );
    },
  );
}
