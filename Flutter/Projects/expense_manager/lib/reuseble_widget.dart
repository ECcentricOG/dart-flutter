import 'package:flutter/material.dart';

Container inputField({
  required TextEditingController userController,
  required bool isPassword,
  required TextInputType type,
  required String hText,
}) {
  return Container(
    height: 40,
    width: 280,
    padding: const EdgeInsets.all(0),
    margin: const EdgeInsets.all(10),
    decoration: const BoxDecoration(
      color: Color.fromRGBO(255, 255, 255, 1),
      boxShadow: [
        BoxShadow(
          color: Color.fromRGBO(0, 0, 0, 0.15),
          blurRadius: 5,
          offset: Offset(0, 3),
        ),
      ],
      borderRadius: BorderRadius.all(
        Radius.circular(8),
      ),
    ),
    child: TextFormField(
      controller: userController,
      obscureText: isPassword,
      keyboardType: type,
      autofocus: false,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.all(10),
        border: InputBorder.none,
        labelText: hText,
        labelStyle: const TextStyle(
          color: Colors.grey,
          fontSize: 12,
        ),
      ),
    ),
  );
}
