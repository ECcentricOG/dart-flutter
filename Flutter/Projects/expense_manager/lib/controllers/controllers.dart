import 'package:flutter/material.dart';

TextEditingController name = TextEditingController();
TextEditingController username = TextEditingController();
TextEditingController password = TextEditingController();
TextEditingController confirm = TextEditingController();

TextEditingController loginUsername = TextEditingController();
TextEditingController loginPassword = TextEditingController();

TextEditingController date = TextEditingController();
TextEditingController amount = TextEditingController();
TextEditingController category = TextEditingController();
TextEditingController description = TextEditingController();

TextEditingController categoryUrl = TextEditingController();
TextEditingController categoryName = TextEditingController();
