class Category {
  final String name;
  final String url;

  const Category({required this.name, required this.url});
}

List<Category> categories = [
  const Category(name: "Food", url: "assets/Mask group.png"),
  const Category(name: "Medical", url: "assets/Mask group(3).png"),
  const Category(name: "Shopping", url: "assets/Mask group(2).png"),
  const Category(name: "Fuel", url: "assets/Mask group(1).png"),
];
