import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management/consumer/app.dart';
import 'package:state_management/consumer/cunsumer_model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          create: (context) => const Player(name: "Leo Messi", jerNo: 18),
        ),
        ChangeNotifierProvider(
          create: (context) => Statistic(goals: 845, assists: 325),
        ),
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: ConsumerApp(),
      ),
    );
  }
}
