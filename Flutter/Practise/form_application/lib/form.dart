import 'package:flutter/material.dart';

class FormWidget extends StatefulWidget {
  const FormWidget({super.key});
  @override
  State createState() => _FormWidgetState();
}

class _FormWidgetState extends State {
  final _formKey = GlobalKey<FormState>();
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Form"),
        centerTitle: true,
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox(
                height: 60,
                width: 300,
                child: TextFormField(
                  key: const ValueKey('username'),
                  validator: (value) {
                    if (value.toString().trim() == '') {
                      return "can't be empty";
                    } else {
                      return null;
                    }
                  },
                  controller: username,
                  obscureText: false,
                  decoration: InputDecoration(
                    hintText: "username",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 60,
                width: 300,
                child: TextFormField(
                  obscureText: true,
                  validator: (value) {
                    if (value!.length <= 7) {
                      return "password must be of 8 letters";
                    } else {
                      return null;
                    }
                  },
                  controller: password,
                  decoration: InputDecoration(
                    hintText: "Password",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () {
                  final isValid = _formKey.currentState!.validate();
                  if (isValid) {
                  } else {}
                },
                child: const Text("Submit"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
