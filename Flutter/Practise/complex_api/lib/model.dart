class User {
  final int? id;
  final String? name;
  final String? email;
  final Address? address;
  final int? phone;
  final String? website;
  final Company company;

  const User({
    required this.address,
    required this.company,
    required this.email,
    required this.id,
    required this.name,
    required this.phone,
    required this.website,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        address: address,
        company: company,
        email: email,
        id: id,
        name: name,
        phone: phone,
        website: website,
      );
}

class Address {
  final String? street;
  final String? suite;
  final String? city;
  final int? zipcode;
  final Geo? geo;

  const Address({
    required this.city,
    required this.geo,
    required this.street,
    required this.suite,
    required this.zipcode,
  });
}

class Geo {
  final double? lat;
  final double? lng;

  const Geo({
    required this.lat,
    required this.lng,
  });
}

class Company {
  final String? name;
  final String? catchPhase;
  final String? bs;

  const Company({
    this.bs,
    this.catchPhase,
    this.name,
  });
}
