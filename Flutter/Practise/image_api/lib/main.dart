import 'package:flutter/material.dart';
import 'package:image_api/data_display.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ImageData(),
      debugShowCheckedModeBanner: false,
    );
  }
}
