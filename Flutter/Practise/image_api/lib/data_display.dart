import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:image_api/model.dart';
import 'package:http/http.dart' as http;

class ImageData extends StatefulWidget {
  const ImageData({super.key});

  @override
  State<ImageData> createState() => _ImageDataState();
}

class _ImageDataState extends State<ImageData> {
  List<Post> allPosts = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.white,
      ),
      body: FutureBuilder(
        future: getData(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: allPosts.length,
              itemBuilder: (context, index) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(20),
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(
                                "${allPosts[index].thumbnailUrl}",
                              ),
                            ),
                            shape: BoxShape.circle,
                            color: Colors.grey,
                            boxShadow: const [
                              BoxShadow(
                                blurRadius: 20,
                                color: Colors.white,
                                blurStyle: BlurStyle.normal,
                                spreadRadius: 10,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        SizedBox(
                          width: 250,
                          child: Text(
                            "${allPosts[index].title}",
                            maxLines: 1,
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        decoration: const BoxDecoration(
                          color: Colors.black,
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 20,
                              color: Colors.white,
                              blurStyle: BlurStyle.normal,
                              spreadRadius: 10,
                            ),
                          ],
                        ),
                        child: Image.network(
                          "${allPosts[index].url}",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ],
                );
              },
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Future<List<Post>> getData() async {
    final response = await http
        .get(Uri.parse("https://jsonplaceholder.typicode.com/photos"));

    var data = jsonDecode(response.body.toString());
    if (response.statusCode == 200) {
      for (Map<String, dynamic> index in data) {
        allPosts.add(Post.fromJson(index));
      }
      return allPosts;
    } else {
      return allPosts;
    }
  }
}
