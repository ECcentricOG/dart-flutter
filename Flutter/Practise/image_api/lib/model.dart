class Post {
  final int? albumId;
  final int? id;
  final String? title;
  final String? url;
  final String? thumbnailUrl;

  const Post({
    required this.albumId,
    required this.id,
    required this.thumbnailUrl,
    required this.title,
    required this.url,
  });

  factory Post.fromJson(Map<String, dynamic> json) => Post(
        albumId: json['albumId'],
        id: json['id'],
        thumbnailUrl: json['thumbnailUrl'],
        title: json['title'],
        url: json['url'],
      );

  Map<String, dynamic> toJson() => {
        "albumId": albumId,
        "id": id,
        "thumbnailUrl": thumbnailUrl,
        "title": title,
        "url": url,
      };
}
