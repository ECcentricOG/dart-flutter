import 'package:flutter/material.dart';
import 'package:flutter_30_day_challage/widgets/list_and_grid.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const ListAndGrid(),
      theme: ThemeData.dark(),
      themeAnimationCurve: Curves.linear,
      themeAnimationDuration: const Duration(
        seconds: 3,
      ),
    );
  }
}
