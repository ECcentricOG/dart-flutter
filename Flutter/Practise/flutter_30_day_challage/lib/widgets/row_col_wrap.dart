import 'package:flutter/material.dart';

class RowColWrap extends StatelessWidget {
  const RowColWrap({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Row Column Wrap"),
        backgroundColor: Colors.deepPurple,
        centerTitle: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            height: 310,
            color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.red,
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.green,
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.blue,
                ),
                const Text(
                  "Row",
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.brown,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 310,
            width: 445,
            color: Colors.purpleAccent,
            child: Wrap(
              alignment: WrapAlignment.start,
              direction: Axis.horizontal,
              children: [
                Container(
                  margin: const EdgeInsets.all(10),
                  height: 120,
                  width: 120,
                  color: Colors.red,
                ),
                Container(
                  margin: const EdgeInsets.all(10),
                  height: 120,
                  width: 120,
                  color: Colors.green,
                ),
                Container(
                  margin: const EdgeInsets.all(10),
                  height: 120,
                  width: 120,
                  color: Colors.blue,
                ),
                Container(
                  margin: const EdgeInsets.all(10),
                  height: 120,
                  width: 120,
                  color: Colors.yellow,
                ),
                const Text(
                  "Wrap",
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.brown,
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 445,
            height: 310,
            color: Colors.blueAccent,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.all(5),
                  height: 70,
                  width: 70,
                  color: Colors.red,
                ),
                Container(
                  margin: const EdgeInsets.all(5),
                  height: 70,
                  width: 70,
                  color: Colors.green,
                ),
                Container(
                  margin: const EdgeInsets.all(5),
                  height: 70,
                  width: 70,
                  color: Colors.yellow,
                ),
                const Text(
                  "Column",
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.brown,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
