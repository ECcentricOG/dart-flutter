import 'package:flutter/material.dart';

class Buttons extends StatelessWidget {
  const Buttons({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Buttons",
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.blueAccent),
                overlayColor: MaterialStateProperty.all(Colors.deepPurple),
                elevation: MaterialStateProperty.all(5),
                foregroundColor: MaterialStateProperty.all(Colors.white),
                shadowColor: MaterialStateProperty.all(Colors.yellow),
                padding: MaterialStateProperty.all(
                  const EdgeInsets.all(20),
                ),
              ),
              onPressed: () {},
              child: const Text(
                "Press me",
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.amber,
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              height: 40,
              width: 200,
              child: ElevatedButton(
                style: const ButtonStyle(
                  shadowColor: MaterialStatePropertyAll(Colors.white),
                  overlayColor: MaterialStatePropertyAll(Colors.deepPurple),
                ),
                onPressed: () {},
                child: const Text('Tap here'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
