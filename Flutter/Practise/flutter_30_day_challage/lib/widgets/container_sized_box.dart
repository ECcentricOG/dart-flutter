import 'package:flutter/material.dart';

class ContainerSizedBox extends StatefulWidget {
  const ContainerSizedBox({super.key});
  @override
  State<ContainerSizedBox> createState() => _ContainerSizedBoxState();
}

class _ContainerSizedBoxState extends State<ContainerSizedBox> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*body: Center(
        child: SizedBox(
          height: 100,
          width: 100,
          child: Text("ECcentricOG"),
        ),
      ),*/

      body: Center(
        child: Container(
          height: 125,
          width: 220,
          //color: Colors.red,
          decoration: const BoxDecoration(
            color: Colors.deepOrangeAccent,
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.white,
                spreadRadius: 1,
                blurRadius: 100,
                blurStyle: BlurStyle.normal,
              ),
            ],
          ),
          padding: const EdgeInsets.all(10),
          child: Center(
            child: Container(
              //color: Colors.orange,
              margin: const EdgeInsets.all(5),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
              ),
              child: const Center(
                child: Text(
                  "ECcentricOG",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
