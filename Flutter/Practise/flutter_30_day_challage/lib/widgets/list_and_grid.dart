import 'package:flutter/material.dart';

class ListAndGrid extends StatefulWidget {
  const ListAndGrid({super.key});
  @override
  State<ListAndGrid> createState() => _ListAndGridState();
}

class _ListAndGridState extends State<ListAndGrid> {
  List<int> list = [];
  int count = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("List and Grid"),
      ),
      /*body: ListView(
        children: [
          Container(
            height: 300,
            width: 200,
            margin: const EdgeInsets.all(10),
            color: Colors.white,
          ),
          Container(
            height: 300,
            width: 200,
            margin: const EdgeInsets.all(10),
            color: Colors.white,
          ),
          Container(
            height: 300,
            width: 200,
            margin: const EdgeInsets.all(10),
            color: Colors.white,
          ),
          Container(
            height: 300,
            width: 200,
            margin: const EdgeInsets.all(10),
            color: Colors.white,
          ),
        ],
      ),*/
      /*body: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          return Container(
            color: Colors.grey,
            child: Text("$index"),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          list.add("");
        },
      ),*/
      /*body: Center(
        child: GridView(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2),
          children: [
            Card(
              color: Colors.grey,
            ),
            Card(
              color: Colors.grey,
            ),
            Card(
              color: Colors.grey,
            ),
            Card(
              color: Colors.grey,
            ),
            Card(
              color: Colors.grey,
            ),
            Card(
              color: Colors.grey,
            ),
            Card(
              color: Colors.grey,
            ),
            Card(
              color: Colors.grey,
            ),
          ],
        ),
      ),*/
      body: Center(
        child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10,
          ),
          itemCount: list.length,
          itemBuilder: (context, index) {
            return SizedBox(
              height: 40,
              width: 120,
              child: Container(
                color: Colors.grey,
                child: Text("$index and $count"),
              ),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          count++;
          list.add(count);
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
