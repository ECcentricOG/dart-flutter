import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Educourse extends StatefulWidget {
  const Educourse({super.key});

  @override
  State<Educourse> createState() => _EducourseState();
}

class _EducourseState extends State<Educourse> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(
            Icons.menu,
            color: Colors.black,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.notifications_none,
              color: Colors.black,
            ),
          ),
        ],
        backgroundColor: Colors.grey.shade300,
      ),
      backgroundColor: Colors.grey.shade300,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(left: 15),
            child: Column(
              children: [
                Text(
                  "Welcome to New",
                  style: GoogleFonts.jost(
                    fontSize: 27,
                    fontWeight: FontWeight.w300,
                  ),
                ),
                Text(
                  "Educourse",
                  style: GoogleFonts.jost(
                    fontSize: 37,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Center(
            child: SizedBox(
              width: 320,
              height: 57,
              child: SearchBar(
                hintText: "Enter your keybord",
                backgroundColor: const MaterialStatePropertyAll(Colors.white),
                trailing: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.search_outlined),
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          Container(
            height: MediaQuery.of(context).size.height - 240,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40),
                topRight: Radius.circular(40),
              ),
              color: Colors.white,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Container(
                  margin: const EdgeInsets.only(
                    left: 20,
                    top: 15,
                  ),
                  child: Text(
                    "Course For You",
                    style: GoogleFonts.jost(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            Container(
                              margin: const EdgeInsets.only(
                                top: 20,
                                bottom: 20,
                                left: 20,
                              ),
                              height: 242,
                              width: 190,
                              decoration: const BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    Color.fromRGBO(197, 4, 98, 1),
                                    Color.fromRGBO(80, 3, 112, 1),
                                  ],
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(14)),
                              ),
                              child: Column(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.all(10),
                                    width: 150,
                                    height: 50,
                                    child: Text(
                                      "UX Designer from Scratch.",
                                      style: GoogleFonts.jost(
                                        fontSize: 17,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  SizedBox(
                                    height: 157,
                                    width: 160,
                                    child: Image.asset(
                                      "assets/7010826_3255307.png",
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                ],
                              ),
                            ),
                            //
                            //
                            //
                            //
                            Container(
                              margin: const EdgeInsets.all(25),
                              height: 242,
                              width: 190,
                              decoration: const BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    Color.fromRGBO(0, 77, 228, 1),
                                    Color.fromRGBO(1, 47, 135, 1),
                                  ],
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(14)),
                              ),
                              child: Column(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.all(10),
                                    width: 150,
                                    height: 50,
                                    child: Text(
                                      "Design Thinking The Beginner",
                                      style: GoogleFonts.jost(
                                        fontSize: 17,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  SizedBox(
                                    height: 157,
                                    width: 160,
                                    child: Image.asset(
                                      "assets/Objects.png",
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                    left: 20,
                    top: 5,
                  ),
                  child: Text(
                    "Course By Category",
                    style: GoogleFonts.jost(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      width: 60,
                      height: 65,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 36,
                            width: 36,
                            child: Image.asset(
                              "assets/1.png",
                            ),
                          ),
                          Text(
                            "UI/UX",
                            style: GoogleFonts.jost(
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 60,
                      height: 65,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 36,
                            width: 36,
                            child: Image.asset(
                              "assets/2.png",
                            ),
                          ),
                          Text(
                            "VIRTUAL",
                            style: GoogleFonts.jost(
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 90,
                      height: 65,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 36,
                            width: 36,
                            child: Image.asset(
                              "assets/ILLUSTATION.png",
                              height: 10,
                              width: 10,
                            ),
                          ),
                          Text(
                            "ILLUSTRATON",
                            style: GoogleFonts.jost(
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 60,
                      height: 65,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 36,
                            width: 36,
                            child: Image.asset(
                              "assets/1.png",
                            ),
                          ),
                          Text(
                            "PHOTO",
                            style: GoogleFonts.jost(
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
